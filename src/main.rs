use crossterm::event;
use crossterm::event::{KeyCode, KeyEvent, KeyEventKind};
use pulse::callbacks::ListResult;
use pulse::context::introspect::SourceInfo;
use pulse::context::subscribe::{Facility, InterestMaskSet, Operation};
use pulse::context::Context;
use pulse::mainloop::standard::Mainloop;
use pulse::stream::{PeekResult, Stream};
use pulse::{def, sample, stream};
use ratatui::buffer::Buffer;
use ratatui::layout::{Alignment, Rect};
use ratatui::style::{Color, Style, Stylize};
use ratatui::symbols::{self, border};
use ratatui::text::{Line, Text};
use ratatui::widgets::block::{Position, Title};
use ratatui::widgets::{Block, Borders, LineGauge, Widget};
use ratatui::Frame;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::ops::Deref;
use std::rc::Rc;
use std::sync::mpsc::{RecvTimeoutError, Sender};
use std::time::Duration;
use std::{io, sync, thread, usize};
mod tui;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let terminal = tui::init()?;

    let res = App::new()?.run(terminal);
    tui::restore()?;

    if let Err(err) = res {
        return Err(err);
    }

    return Ok(());
}

struct Source {
    index: u32,
    name: String,
    peak: f32,
}

impl Source {
    fn new(s: &SourceInfo) -> Source {
        let name;
        if let Some(cow_name) = &s.name {
            name = cow_name.deref().to_string();
        } else {
            name = String::new();
        }

        Source {
            index: s.index,
            name,
            peak: 0.,
        }
    }

    fn create_stream(
        &self,
        ctx: &mut Context,
        msg_tx: Sender<Msg>,
    ) -> Result<Rc<RefCell<Stream>>, Box<dyn std::error::Error>> {
        let Some(stream) = Stream::new(ctx, &self.name, &sample_spec(), None) else {
            return Err(Box::from(format!("create stream for {}", self.name)));
        };
        let stream = Rc::new(RefCell::new(stream));

        let stream_ref = stream.clone();
        let idx = self.index;
        (*stream)
            .borrow_mut()
            .set_read_callback(Some(Box::new(move |_| {
                let res = (*stream_ref).borrow_mut().peek();
                match res {
                    Ok(res) => match res {
                        PeekResult::Data(data) => {
                            if data.len() < 4 || data.len() % 4 != 0 {
                                return;
                            }
                            let byte_arr: [u8; 4] = data[data.len() - 4..data.len()]
                                .try_into()
                                .expect("invalid peek data");
                            let sample = f32::from_le_bytes(byte_arr);
                            msg_tx.send(Msg::Read(idx, sample)).unwrap();

                            if let Err(err) = (*stream_ref).borrow_mut().discard() {
                                // eprintln!("stream discard errored: {err}");
                            }
                        }
                        PeekResult::Empty => {}
                        PeekResult::Hole(_) => {
                            if let Err(err) = (*stream_ref).borrow_mut().discard() {
                                // eprintln!("stream discard errored: {err}");
                            }
                        }
                    },
                    Err(err) => {
                        // eprintln!("stream peek failed: {err}");
                        return;
                    }
                }
            })));
        let stream_ref = stream.clone();
        (*stream)
            .borrow_mut()
            .set_state_callback(Some(Box::new(move || {
                let Ok(stream) = (*stream_ref).try_borrow() else {
                    // eprintln!("skipping stream state callback because already borrowed");
                    return;
                };

                let state = stream.get_state();
                let name = stream.borrow().get_device_name();
                // eprintln!("stream to {name:?} at state {state:?}");
            })));
        (*stream).borrow_mut().connect_record(
            Some(&self.name),
            Some(&def::BufferAttr {
                maxlength: std::u32::MAX,
                fragsize: 4, // f32
                tlength: std::u32::MAX,
                minreq: std::u32::MAX,
                prebuf: std::u32::MAX,
            }),
            stream_flag(),
        )?;

        Ok(stream)
    }
}

struct App {
    streams: Rc<RefCell<HashMap<u32, Rc<RefCell<Stream>>>>>,
    mainloop: Mainloop,
    context: Rc<RefCell<Context>>,
}

fn sample_spec() -> sample::Spec {
    return sample::Spec {
        format: sample::Format::F32le,
        rate: 60,
        channels: 1,
    };
}

fn stream_flag() -> stream::FlagSet {
    return stream::FlagSet::DONT_MOVE
        | stream::FlagSet::ADJUST_LATENCY
        | stream::FlagSet::PEAK_DETECT;
}

enum Msg {
    Source(Source),
    SourceRemove(u32),
    Read(u32, f32),
}

impl App {
    pub fn new() -> Result<App, Box<dyn std::error::Error>> {
        let Some(mainloop) = Mainloop::new() else {
            return Err(String::from("allocate mainloop failed").into());
        };

        let Some(context) = Context::new(&mainloop, "pavoltui") else {
            return Err(String::from("allocate context failed").into());
        };

        let context = Rc::new(RefCell::new(context));

        return Ok(App {
            mainloop,
            context,
            streams: Rc::new(RefCell::new(HashMap::new())),
        });
    }

    pub fn run(&mut self, mut terminal: tui::Tui) -> Result<(), Box<dyn std::error::Error>> {
        let (msg_tx, msg_rx) = sync::mpsc::channel();
        {
            let context_ref = Rc::clone(&self.context);
            let sources = self.streams.clone();
            let msg_tx = msg_tx.clone();
            let cb = move || {
                // eprintln!("statecb");
                let Ok(mut ctx) = (*context_ref).try_borrow_mut() else {
                    // eprintln!("skipping");
                    return;
                };
                let state = ctx.get_state();
                // eprintln!("state: {state:?}");
                match state {
                    pulse::context::State::Ready => {
                        let streams = sources.clone();
                        let context_ref = context_ref.clone();
                        let msg_tx = msg_tx.clone();
                        ctx.introspect().get_source_info_list(move |x| match x {
                            ListResult::Item(item) => {
                                // eprintln!("list item {:?}", item.name);
                                let mut ctx = (*context_ref).borrow_mut();
                                let source = Source::new(item);
                                let stream = source.create_stream(&mut ctx, msg_tx.clone());
                                match stream {
                                    Ok(stream) => {
                                        (*streams).borrow_mut().insert(item.index, stream);
                                    }
                                    Err(err) => {
                                        // eprintln!("create source: {err}");
                                        return;
                                    }
                                }
                                msg_tx.send(Msg::Source(source)).unwrap();
                            }
                            ListResult::End => {}
                            ListResult::Error => {
                                println!("reaches list error");
                            }
                        });

                        ctx.subscribe(InterestMaskSet::SOURCE, |succeeded| {
                            if !succeeded {
                                // eprintln!("subscribe failed");
                            }
                        });
                    }
                    _ => {}
                }
            };
            RefCell::borrow_mut(Rc::borrow(&self.context)).set_state_callback(Some(Box::new(cb)));
            // eprintln!("set_state_callback");
        }

        {
            let ctx = Rc::clone(&self.context);
            let mut ctx = (*ctx).borrow_mut();
            ctx.connect(None, pulse::context::FlagSet::NOFLAGS, None)?;
            // eprintln!("connect");

            {
                let ctx_ref = Rc::clone(&self.context);
                let streams_ref = self.streams.clone();
                let msg_tx = msg_tx.clone();
                ctx.set_subscribe_callback(Some(Box::new(move |facility, op, idx| {
                    let Some(facility) = facility else {
                        // eprintln!("unknown facility");
                        return;
                    };

                    let Some(op) = op else {
                        // eprintln!("unknown op");
                        return;
                    };

                    let ctx = (*ctx_ref).borrow();

                    if facility == Facility::Source {
                        match op {
                            Operation::Removed => {
                                msg_tx.send(Msg::SourceRemove(idx)).unwrap();
                                if let Some(stream) = (*streams_ref).borrow_mut().remove(&idx) {
                                    if let Err(err) = (*stream).borrow_mut().disconnect() {
                                        // eprintln!("failed to disconnect stream: {err}");
                                    }
                                };
                            }
                            Operation::New | Operation::Changed => {
                                let streams_ref = streams_ref.clone();
                                let ctx_ref = Rc::clone(&ctx_ref);
                                let msg_tx = msg_tx.clone();
                                ctx.introspect()
                                    .get_source_info_by_index(idx, move |s| match s {
                                        ListResult::Item(item) => {
                                            let mut ctx = (*ctx_ref).borrow_mut();
                                            let source = Source::new(item);
                                            let stream =
                                                source.create_stream(&mut ctx, msg_tx.clone());
                                            match stream {
                                                Ok(stream) => {
                                                    (*streams_ref)
                                                        .borrow_mut()
                                                        .insert(item.index, stream);
                                                }
                                                Err(err) => {
                                                    // eprintln!("failed to create source: {err}");
                                                    return;
                                                }
                                            }
                                            msg_tx.send(Msg::Source(source)).unwrap();
                                        }
                                        ListResult::End => {}
                                        ListResult::Error => {
                                            // eprintln!("get source info by index failed");
                                        }
                                    });
                            }
                        }
                    }

                    // eprintln!("facility: {facility:?}, op: {op:?}, idx: {idx:?}");
                })));
            }
        }

        let mut widget = AppWidget::new();
        let (done_tx, done_rx) = sync::mpsc::channel();
        let handle = thread::spawn(move || {
            'outer: while !widget.exit {
                loop {
                    let res = msg_rx.recv_timeout(Duration::ZERO);
                    match res {
                        Ok(msg) => match msg {
                            Msg::Source(s) => {
                                widget.sources.insert(s.index, s);
                                ();
                            }
                            Msg::SourceRemove(index) => {
                                widget.sources.remove(&index);
                                ();
                            }
                            Msg::Read(idx, peak) => {
                                if let Some(s) = widget.sources.get_mut(&idx) {
                                    s.peak = peak;
                                }
                            }
                        },
                        Err(err) => match err {
                            RecvTimeoutError::Timeout => break,
                            RecvTimeoutError::Disconnected => break 'outer,
                        },
                    }
                }

                if let Err(err) = terminal.draw(|frame| widget.render_frame(frame)) {
                    // eprintln!("error draw {err}");
                    break;
                }
                if let Err(err) = widget.handle_events() {
                    // eprintln!("error handle events {err}");
                    break;
                };
            }
            done_tx.send(true).unwrap();
        });

        loop {
            match done_rx.recv_timeout(Duration::from_secs(0)) {
                Ok(done) => {
                    if done {
                        break;
                    }
                }
                Err(err) => match err {
                    RecvTimeoutError::Timeout => {}
                    RecvTimeoutError::Disconnected => {
                        return Err(Box::new(err));
                    }
                },
            };
            self.mainloop
                .prepare(Some(pulse::time::MicroSeconds(16_000)))?;
            self.mainloop.poll()?;
            self.mainloop.dispatch()?;
        }

        if let Err(err) = handle.join() {
            return Err(Box::from(format!("err when joining {err:?}")));
        };

        return Ok(());
    }
}

struct AppWidget {
    sources: BTreeMap<u32, Source>,
    curr: u32,
    exit: bool,
}

impl AppWidget {
    fn new() -> AppWidget {
        AppWidget {
            sources: BTreeMap::new(),
            curr: 0,
            exit: false,
        }
    }

    fn exit(&mut self) {
        self.exit = true;
    }

    fn render_frame(&mut self, frame: &mut Frame) {
        frame.render_widget(self, frame.size());
    }

    fn handle_events(&mut self) -> io::Result<()> {
        if event::poll(Duration::from_millis(16))? {
            if let event::Event::Key(evt) = event::read()? {
                if evt.kind == KeyEventKind::Press {
                    self.handle_key_event(evt);
                }
            }
        }

        return Ok(());
    }

    fn handle_key_event(&mut self, evt: KeyEvent) {
        match evt.code {
            KeyCode::Char('q') => self.exit(),
            KeyCode::Char('k') | KeyCode::Up => self.up(),
            KeyCode::Char('j') | KeyCode::Down => self.down(),
            _ => {}
        }
    }

    fn get_start(&self) -> Option<u32> {
        for key in self.sources.keys() {
            if *key >= self.curr {
                return Some(*key);
            }
        }
        return None;
    }

    fn up(&mut self) {
        let Some(start) = self.get_start() else {
            if let Some((k, _)) = self.sources.last_key_value() {
                self.curr = *k;
            }
            return;
        };

        for key in self.sources.keys().rev() {
            if *key < start {
                self.curr = *key;
                return;
            }
        }

        if let Some((k, _)) = self.sources.first_key_value() {
            self.curr = *k;
        }
    }

    fn down(&mut self) {
        let Some(start) = self.get_start() else {
            if let Some((k, _)) = self.sources.last_key_value() {
                self.curr = *k;
            }
            return;
        };

        for key in self.sources.keys() {
            if *key > start {
                self.curr = *key;
                return;
            }
        }

        if let Some((k, _)) = self.sources.first_key_value() {
            self.curr = *k;
        }
    }
}

impl Widget for &mut AppWidget {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let title = Title::from(" PulseAudio volume meter ".bold());
        let instructions = Title::from(Line::from(vec![
            " Quit ".into(),
            "<Q> ".blue().bold(),
            "Up ".into(),
            "<Up/k> ".blue().bold(),
            "Down ".into(),
            "<Down/j> ".blue().bold(),
        ]));
        let block = Block::default()
            .title(title.alignment(Alignment::Center))
            .title(
                instructions
                    .alignment(Alignment::Center)
                    .position(Position::Bottom),
            )
            .borders(Borders::ALL)
            .border_set(border::THICK);

        let mut inner = block.inner(area);
        block.render(area, buf);

        // Remove gaps from bottom
        if self.sources.len() * 2 < inner.height as usize {
            self.curr = 0
        }

        for (i, idx) in self.sources.keys().rev().enumerate() {
            if i + 1 == inner.height as usize / 2 && self.curr > *idx {
                self.curr = *idx
            }
        }

        for (idx, src) in self.sources.iter() {
            if *idx < self.curr {
                continue;
            }

            if inner.height == 0 {
                break;
            }

            let text = Text::from(src.name.clone());
            let h = text.height() as u16;
            text.render(inner, buf);
            inner.height -= h;
            inner.y += h;

            if inner.height == 0 {
                break;
            }

            let gauge = LineGauge::default()
                .label("")
                .gauge_style(Style::default().fg(Color::Blue).bg(Color::Black))
                .line_set(symbols::line::THICK)
                .ratio(src.peak as f64);
            gauge.render(inner, buf);
            inner.height -= 1;
            inner.y += 1;
        }
    }
}
